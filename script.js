document.querySelector('.tabs').addEventListener('click', changingTab);

let tabTitle = document.getElementsByClassName('tabs-title');
let tabText = document.getElementsByClassName('tabs-text');

tabTitle[0].classList.add('active');
tabText[0].style.display = 'block';

function changingTab(event) {
  let dataTab = event.target.getAttribute('data-tab');

  if (event.target.className == 'tabs-title') {
    for (let i = 0; i < tabTitle.length; i++) {
      tabTitle[i].classList.remove('active');
    }

    event.target.classList.add('active');

    for (let i = 0; i < tabText.length; i++) {
      if (dataTab == i) {
        tabText[i].style.display = 'block';
      } else {
        tabText[i].style.display = 'none';
      }
    }
  }
}

